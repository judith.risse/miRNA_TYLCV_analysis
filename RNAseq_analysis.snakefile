SAMPLE=['MM_TYLC2', 'Ty1_H2', 'MM_H3', 'Ty1_H3', 'Ty1TYLC1', 'MM_TYLC1', 'MM_TYLC3', 'MM_H2', 'Ty1TYLC4', 'Ty1_H1', 'MM_H1', 'Ty1TYLC6']
READS= ['1','2']

rule all:
    input:
        "multiqc_report.html"

rule fastqc_raw:
    input:
        fastq1="rawdata/{SAMPLE}_1.fq.gz",
        fastq2="rawdata/{SAMPLE}_2.fq.gz"
    output:
        "fastqc/{SAMPLE}_1_fastqc.html",
        "fastqc/{SAMPLE}_1_fastqc.zip",
        "fastqc/{SAMPLE}_2_fastqc.html",
        "fastqc/{SAMPLE}_2_fastqc.zip"
    container: 'resources/fastqc_0.11.9--hdfd78af_1.sif'
    shell:
        "fastqc -o fastqc {input.fastq1} {input.fastq2}"

rule trim_cutadapt:
    input:
        ["rawdata/{SAMPLE}_1.fq.gz","rawdata/{SAMPLE}_2.fq.gz"]
    output:
        fastq1="trimmed/{SAMPLE}_trimmed_1.fq.gz",
        fastq2="trimmed/{SAMPLE}_trimmed_2.fq.gz",
        clog="{SAMPLE}_cutadapt.log"
    container: "resources/cutadapt_3.5--py36hc5360cc_0.sif"
    shell:
        "cutadapt -a AGATCGGAAGAGC -A AGATCGGAAGAGC -m 50 -q 30 -o {output.fastq1} -p {output.fastq2} {input} > {output.clog}"

rule align_STAR:
    input:
        fastq1="trimmed/{SAMPLE}_trimmed_1.fq.gz",
        fastq2="trimmed/{SAMPLE}_trimmed_2.fq.gz",
    output:
        "star/{SAMPLE}_trimmed_STAR.Log.final.out",
        "star/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.out.bam",
    params:
        genomeDir="references/tomato_TYLCV",
        outfile_prefix="star/{SAMPLE}_trimmed_STAR."
    threads: 2
    container: "resources/star_2.7.9a--h9ee0642_0.sif"
    shell:
        "STAR --runThreadN {threads} --genomeDir {params.genomeDir} --readFilesCommand gunzip -c --genomeLoad NoSharedMemory --quantMode GeneCounts --outSAMtype BAM SortedByCoordinate --outFileNamePrefix {params.outfile_prefix} --readFilesIn {input.fastq1} {input.fastq2} "

rule index_STAR_samtools:
    input:
        "star/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.out.bam"
    output:
        "star/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.out.bam.bai"
    container:"resources/samtools_1.14--hb421002_0.sif"
    shell:
        "samtools index {input}"


rule mark_duplicates_picard:
    input:
        bam="star/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.out.bam",
        bai="star/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.out.bam.bai"
    output:
        bam="mrkdup/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam",
        metrics="reports/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam.metrics"
    container: "resources/picard_latest.sif"
    shell:
        "java -jar /usr/picard/picard.jar MarkDuplicates OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 TAGGING_POLICY=OpticalOnly I={input.bam} O={output.bam} M={output.metrics}"

rule index_markdups_samtools:
    input:
        "mrkdup/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam"
    output:
        "mrkdup/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam.bai"
    container:"resources/samtools_1.14--hb421002_0.sif"
    shell:
        "samtools index {input}"

rule collect_RNAseq_metrics_picard:
    input:
        bam="mrkdup/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam",
        bai="mrkdup/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam.bai"
    output:
        pdf="reports/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.RNAseqMetrics.pdf",
        stats="reports/{SAMPLE}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.RNAseqMetrics.stats"
    params:
        refflat="references/S_lycopersicum_4.gp.picard"
    container: "resources/picard_latest.sif"
    shell:
        "java -jar /usr/picard/picard.jar CollectRnaSeqMetrics REF_FLAT={params.refflat} STRAND=SECOND_READ_TRANSCRIPTION_STRAND CHART={output.pdf} I={input.bam} O={output.stats}"

rule quant_salmon:
    input:
        fastq1="trimmed/{SAMPLE}_trimmed_1.fq.gz",
        fastq2="trimmed/{SAMPLE}_trimmed_2.fq.gz",
    output:
        "{SAMPLE}_salmon/quant.sf",
        "{SAMPLE}_salmon/libParams/flenDist.txt"
    params:
        index="references/S_lycopersicum_4_index",
        outfolder="{SAMPLE}_salmon"
    threads: 6
    container: "resources/salmon_1.6.0.sif"
    shell:
        "salmon quant -i {params.index} -l A -p {threads} --validateMappings --gcBias --numGibbsSamples 20 -o {params.outfolder} -1 {input.fastq1} -2 {input.fastq2}"

rule quant_salmon_gb:
    input:
        fastq1="trimmed/{SAMPLE}_trimmed_1.fq.gz",
        fastq2="trimmed/{SAMPLE}_trimmed_2.fq.gz",
    output:
        "{SAMPLE}_salmon_gb/quant.sf",
        "{SAMPLE}_salmon_gb/libParams/flenDist.txt"
    params:
        index="references/S_lycopersicum_4_index",
        outfolder="{SAMPLE}_salmon_gb"
    threads: 6
    container: "resources/salmon_1.6.0.sif"
    shell:
        "salmon quant -i {params.index} -l A -p {threads} --validateMappings --gcBias --numGibbsSamples 20 -o {params.outfolder} -1 {input.fastq1} -2 {input.fastq2}"

rule MultiQC:
    input:
        expand("{samples}_salmon/libParams/flenDist.txt",samples=SAMPLE),
        expand("{samples}_salmon_gb/libParams/flenDist.txt",samples=SAMPLE),
        expand("reports/{samples}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.RNAseqMetrics.stats",samples = SAMPLE),
        expand("reports/{samples}_trimmed_STAR.Aligned.sortedByCoord.mrkdup.bam.metrics",samples = SAMPLE),
        expand("star/{samples}_trimmed_STAR.Log.final.out",samples=SAMPLE),
        expand("fastqc/{samples}_{reads}_fastqc.html",samples=SAMPLE, reads=READS),
    output:
        "multiqc_report.html"
    container: "resources/multiqc_1.9.sif"
    shell:
        "multiqc --tag RNA ./"
