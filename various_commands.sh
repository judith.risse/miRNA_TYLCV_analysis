!#/usr/bin/bash

# commandline for running sRNAde on the results of sRNAbench
singularity exec --bind $PWD srnatoolbox_0.0.6.sif java -jar /opt/sRNAtoolboxDB/exec/sRNAde.jar input=sRNAbench/out output=sRNAbench/sRNAde grpString=MM-EA1_S1_miRNA:MM-EA2_S2_miRNA:MM-EA3_S3_miRNA#Ty1-EA1_S4_miRNA:Ty1-EA2_S5_miRNA:Ty1-EA3_S6_miRNA#MM-TYLCV-1_S7_miRNA:MM-TYLCV-2_S8_miRNA:MM-TYLCV-3_S9_miRNA#Ty1-TYLCV-1_S10_miRNA:Ty1-TYLCV-4_S11_miRNA:Ty1-TYLCV-6_S12_miRNA sampleDesc=MM-EA1:MM-EA2:MM-EA3:Ty1-EA1:Ty1-EA2:Ty1-EA3:MM-TYLCV-1:MM-TYLCV-2:MM-TYLCV-3:Ty1-TYLCV-1:Ty1-TYLCV-4:Ty1-TYLCV-6 grpDesc=MM-EA#Ty1-EA#MM-TYLCV#Ty1-TYLCV diffExpr=true dbPath=sRNAbench stat=true

# collection of miRNA target genes and conversion to ITAG 4.0 gene IDs for significantly differentially expressed miRNAs for each of the three models
# interaction model
grep -Fwf <(cut -f 1 DE_miRNA_interaction.txt) ../references/Solanum_lycopersicum_targetGene.txt | cut -f 2 | sort | uniq > miRNATargets_interaction.txt
grep -Fwf <(cat miRNATargets_interaction.txt| sed 's/....$//') ../references/ITAG4.0_gene_models.gff | grep -P '\tmRNA\t' | cut -f 9 | sed 's/;Parent=.*Note=/\t/;s/^ID=mRNA://;s/;.*$//;s/..\t/\t/;s/ (AHRD.*\* /; /;s/)$//' > miRNATargets_interaction_ITAG4.txt
# plant model
grep -Fwf <(cut -f 1 DE_miRNA_plant.txt) ../references/Solanum_lycopersicum_targetGene.txt | cut -f 2 | sort | uniq > miRNATargets_plant.txt
grep -Fwf <(cat miRNATargets_plant.txt| sed 's/....$//') ../references/ITAG4.0_gene_models.gff | grep -P '\tmRNA\t' | cut -f 9 | sed 's/;Parent=.*Note=/\t/;s/^ID=mRNA://;s/;.*$//;s/..\t/\t/;s/ (AHRD.*\* /; /;s/)$//' > miRNATargets_plant_ITAG4.txt
# treatment model
grep -Fwf <(cut -f 1 DE_miRNA_treatment.txt) ../references/Solanum_lycopersicum_targetGene.txt | cut -f 2 | sort | uniq > miRNATargets_treatment.txt
grep -Fwf <(cat miRNATargets_treatment.txt| sed 's/....$//') ../references/ITAG4.0_gene_models.gff | grep -P '\tmRNA\t' | cut -f 9 | sed 's/;Parent=.*Note=/\t/;s/^ID=mRNA://;s/;.*$//;s/..\t/\t/;s/ (AHRD.*\* /; /;s/)$//' > miRNATargets_treatment_ITAG4.txt
