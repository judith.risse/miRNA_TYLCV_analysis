## Using sRNAbench for prediction of novel miRNAs and counts based analysis

samples = {'MM-EA1_S1', 'MM-EA2_S2', 'MM-EA3_S3', 'Ty1-EA1_S4', 'Ty1-EA2_S5', 'Ty1-EA3_S6', 'MM-TYLCV-1_S7', 'MM-TYLCV-2_S8', 'MM-TYLCV-3_S9', 'Ty1-TYLCV-1_S10', 'Ty1-TYLCV-4_S11', 'Ty1-TYLCV-6_S12'}

rule all:
    input:
        expand("sRNAbench/out/{sample}_miRNA/mature_sense.grouped", sample=samples),
        expand("sRNAbench/out/{sample}_miRNA/homologmature_sense.grouped",sample=samples),

rule sRNAbench_pre:
    input:
        sample="rawdata/{sample}_R1_001.fastq.gz",
    output:
        reads="sRNAbench/out/{sample}_pre/reads_orig.fa",
        outdir="sRNAbench/out/{sample}_pre/",
    params:
        param="adapterMinLength=6 adapter=AACTGTAGGCACCATCAAT umi=3pA12",
    log:
        "logs/sRNAbench_{sample}_pre.log"
    shell:
        """java -jar resources/sRNAbench.jar input={input.sample} output={output.outdir} {params.param} &>{log} """


rule sRNAbench_predict:
    input:
        reads="sRNAbench/out/{sample}_pre/reads_orig.fa",
    output:
        counts="sRNAbench/out/{sample}_miRNA/mature_sense.grouped",
        mature="sRNAbench/out/{sample}_miRNA/homologmature_sense.grouped",
        reads="sRNAbench/out/{sample}_miRNA/stat/unAssignedReads.fa",
    threads:
        10
    params:
        param="microRNA=Sly homolog=stu species=S_lycopersicum_4:TYLCV kingdom=plant minReadLength=19 maxReadLength=24 isoMiR=true libs=SL3_tRNA.gff libs=SL3_rRNA.gff libs=SL3_snRNA.gff libs=SL3_snoRNA.gff libs=S_lycopersicum_4_TAS.bed splitToSpecies=true bedGraph=true",
        outdir="sRNAbench/out/{sample}_miRNA",
    log:
        "logs/sRNAbench_{sample}_pre.log"
    shell:
        """java -jar sRNAbench.jar input={input.reads} output={params.outdir} {params.param} -p {threads} dbPath=sRNAbench &>{log} """
        
